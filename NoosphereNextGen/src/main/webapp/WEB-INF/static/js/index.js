/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    var host = window.location.protocol + '//' + window.location.host + "/NoosphereNextGen";
    var userinfo = host + "/userinfo/";
    var levelinfo = host + "/levelinfo/";
    var setinfo = host + "/setinfo";

    getUserResults();
    getLevelResults();
    setInfo();

    function getUserResults() {
        $(".get-user-button").click(function () {
            var user = $(".get-user-input").val();
            $.getJSON(userinfo + user, function (data) {
                var text = '[';
                $.each(data, function (index, item) {
                    if (index !== 0) {
                        text += ',';
                    }
                    text += '{"level" : "' + item.level + '", "result" : ' + item.result + '}';
                });
                text += ']';
                $(".user-result").text(text);
            });
        });
    }

    function getLevelResults() {
        $(".get-level-button").click(function () {
            var level = $(".get-level-input").val();
            $.getJSON(levelinfo + level, function (data) {
                var text = '[';
                $.each(data, function (index, item) {
                    if (index !== 0) {
                        text += ',';
                    }
                    text += '{"user" : "' + item.user + '", "result" : ' + item.result + '}';
                });
                text += ']';
                $(".level-result").text(text);
            });
        });
    }

    function setInfo() {
        $(".set-button").click(function () {
            var user = $(".set-user").val();
            var level = $(".set-level").val();
            var result = $(".set-result").val();
            var myData = '{"level":"' + level + '", "result":' + result + ', "user":"' + user + '"}';
            console.log(myData);
            $.ajax({
                type: "PUT",
                url: setinfo,
                dataType: 'json',
                contentType: "application/json",
                data: myData,
                success: function () {
                }
            });
        });
    }
});
