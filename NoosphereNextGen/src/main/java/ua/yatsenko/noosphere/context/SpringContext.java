/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.context;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */

@Configuration
@EnableWebMvc
@ComponentScan("ua.yatsenko.noosphere")
public class SpringContext extends WebMvcConfigurerAdapter{
    
    @Bean
    public Config config(){
        Config cfg = new Config();
        cfg.getGroupConfig().setName("dev").setPassword("dev");
        cfg.getNetworkConfig().setPort(5701).setPortAutoIncrement(true).setPortCount(100);
        cfg.getNetworkConfig().setJoin(
                new JoinConfig().setMulticastConfig(
                        new MulticastConfig().setEnabled(false)).setTcpIpConfig(
                                new TcpIpConfig().setEnabled(true).setMembers(Collections.singletonList("10.1.207.177"))));
        cfg.setProperty("hazelcast.logging.type", "slf4j");
        return cfg;
    }
    
    @Bean
    public HazelcastInstance instance() {
        return Hazelcast.newHazelcastInstance(config());
    }
    
    @Bean("viewResolver")
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/");
        resolver.setSuffix(".html");
        return resolver;
    }
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/html/**").addResourceLocations("/WEB-INF/static/html/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("/WEB-INF/static/fonts/");
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/static/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/static/js/");
        registry.addResourceHandler("/components/**").addResourceLocations("/WEB-INF/static/components/");
    }
}
