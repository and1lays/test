/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.dao;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IdGenerator;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.yatsenko.noosphere.dto.Result;
import ua.yatsenko.noosphere.service.SettingService;
import ua.yatsenko.noosphere.utils.KeyComparator;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class SettingDao {

    private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

    @Autowired
    private HazelcastInstance instance;

    private IMap<Long, Result> resultMap;

    @PostConstruct
    public void init() {
        resultMap = instance.getMap("resultMap");
    }
    
    public Set<Long> keySet(){
        return resultMap.keySet();
    }
    
    public boolean isEmpty(){
        return resultMap.isEmpty();
    }
    
    public boolean containsValue(Result result){
        return resultMap.containsValue(result);
    }
    
    public Result getByKey(Long key){
        return resultMap.get(key);
    }
    
    public void replace(Long key, Result oldResult, Result newResult){
        resultMap.replace(key, oldResult, newResult);
    }
    
    public void set(Long key, Result result){
        resultMap.set(key, result);
    }
    
    public void lock(Long key){
        resultMap.lock(key);
    }
    
    public void unlock(Long key){
        resultMap.unlock(key);
    }

//    public void set(String info) {
//        try {
//
//            JSONParser parser = new JSONParser();
//            JSONObject jsonResult = (JSONObject) parser.parse(info);
//            Result result = new Result((String) jsonResult.get("user"), (String) jsonResult.get("level"), (long) jsonResult.get("result"));
//
//            long nextResultKey = 0;
////            Long nextResultKey = idGen.newId();
//            if (!resultMap.isEmpty()) {
//                nextResultKey = Collections.max(resultMap.keySet(), KeyComparator.getInstance()) + 1;
//            }
//            boolean flag = true;
//
//            resultMap.lock(nextResultKey);
//
//            try {
//                for (Long key : resultMap.keySet()) {
//                    if (resultMap.containsValue(result) || flag == false) {
//                        flag = false;
//                        break;
//                    }
//
//                    Result res = resultMap.get(key);
//                    if (res.getUser().equals(result.getUser()) && res.getLevel().equals(result.getLevel())) {
//                        if (res.getResult() < result.getResult()) {
//                            resultMap.replace(key, res, result);
//                        }
//                        flag = false;
//                    }
//                }
//                if (flag) {
//                    resultMap.put(nextResultKey, result);
//                    if (!userDao.contains(result.getUser())) {
//                        userDao.setUser(result.getUser());
//                    }
//                    if (!levelDao.contains(result.getLevel())) {
//                        levelDao.setLevel(result.getLevel());
//                    }
//                }
//            } finally {
//                resultMap.unlock(nextResultKey);
//            }
//        } catch (ParseException ex) {
//            Logger.getLogger(SettingService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
