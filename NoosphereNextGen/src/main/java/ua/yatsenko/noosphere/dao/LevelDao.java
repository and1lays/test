/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.dao;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ISet;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import java.util.Collection;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.yatsenko.noosphere.dto.Result;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class LevelDao {
    
    @Autowired
    private HazelcastInstance instance;
    
    private ISet<String> levelSet;
    private IMap<Long, Result> resultMap;
    
    @PostConstruct
    public void init() {
        this.levelSet = instance.getSet("levelSet");
        this.resultMap = instance.getMap("resultMap");
    }
    
    public Collection<Result> getResults(String level){
        Predicate equalLevel = Predicates.equal("level", level);
        return resultMap.values(equalLevel);
    }
    
    public synchronized void setLevel(String level){
        levelSet.add(level);
    }
    
    public boolean contains(String level){
        return levelSet.contains(level);
    }
}
