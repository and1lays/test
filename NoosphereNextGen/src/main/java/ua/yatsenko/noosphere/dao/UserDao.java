/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.dao;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ISet;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import java.util.Collection;
import javax.annotation.PostConstruct;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.yatsenko.noosphere.dto.Result;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class UserDao {
    
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HazelcastInstance instance;

    private ISet<String> userSet;
    private IMap<Long, Result> resultMap;

    @PostConstruct
    public void init() {
        this.userSet = instance.getSet("userSet");
        this.resultMap = instance.getMap("resultMap");
    }

    public Collection<Result> getResults(String user) {
        Predicate equalUser = Predicates.equal("user", user);
        return resultMap.values(equalUser);
    }

    public synchronized void setUser(String user) {
        userSet.add(user);
    }

    public boolean contains(String user) {
        return userSet.contains(user);
    }
}
