/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.utils;

import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class KeyComparator implements Comparator<Long>{

    @Override
    public int compare(Long x, Long y) {
        return (x < y) ? -1 : ((Objects.equals(x, y)) ? 0 : 1);
    }

    private KeyComparator() {
    }
    
    private static class Holder {
        private final static KeyComparator INSTANCE = new KeyComparator();
    }
    
    public static KeyComparator getInstance(){
        return Holder.INSTANCE;
    }
}
