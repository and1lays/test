/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.utils;

import java.util.Comparator;
import ua.yatsenko.noosphere.dto.Result;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ResultComparator implements Comparator<Result>{

    @Override
    public int compare(Result result1, Result result2) {
        return (result1.getResult() < result2.getResult()) ? 1 : ((result1.getResult() == result2.getResult()) ? 0 : -1);
    }
    
    private ResultComparator() {
    }
    
    private static class Holder {
        private final static ResultComparator INSTANCE = new ResultComparator();
    }
    
    public static ResultComparator getInstance(){
        return Holder.INSTANCE;
    }
    
}
