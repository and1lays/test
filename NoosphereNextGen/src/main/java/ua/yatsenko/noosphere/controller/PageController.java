/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Controller
public class PageController {
    
    @RequestMapping(value = "/")
    public String index() {
        return "html/index";
    }
}
