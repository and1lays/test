/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.yatsenko.noosphere.dto.Result;
import ua.yatsenko.noosphere.service.LevelService;
import ua.yatsenko.noosphere.service.SettingService;
import ua.yatsenko.noosphere.service.UserService;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@RestController
public class MyRestController {
    
    @Autowired
    private LevelService levelService;
    @Autowired
    private UserService userService;
    @Autowired
    private SettingService settingService;
    
    @RequestMapping(value = "/levelinfo/{level}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getLevelInfo(@PathVariable ("level") String level){
        return levelService.getLevelInfo(level);
    }
    
    @RequestMapping(value = "/userinfo/{user}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getUserInfo(@PathVariable ("user") String user){
        return userService.getUserInfo(user);
    }
    
    @RequestMapping(value = "/setinfo", method = RequestMethod.PUT)
    public void putSetInfo(@RequestBody String info){
        settingService.putInfo(info);
    }
    
}
