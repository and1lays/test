/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.noosphere.dao.UserDao;
import ua.yatsenko.noosphere.dto.Result;
import ua.yatsenko.noosphere.utils.ResultComparator;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 * 
 * Service layer for work with userSet.
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public String getUserInfo(String user) {
        JSONArray jsonArray = new JSONArray();
        if (userDao.contains(user)) {
            List<Result> resultList = new ArrayList<>(userDao.getResults(user));
            Collections.sort(resultList, ResultComparator.getInstance());
            for (Result result : resultList) {
                if (jsonArray.size() < 20) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("level", result.getLevel());
                    jsonObj.put("result", result.getResult());
                    jsonArray.add(jsonObj);
                } else {
                    break;
                }
            }
        } else {
            userDao.setUser(user);
        }
        return jsonArray.toJSONString();
    }
}
