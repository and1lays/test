/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.noosphere.dao.LevelDao;
import ua.yatsenko.noosphere.dto.Result;
import ua.yatsenko.noosphere.utils.ResultComparator;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 * 
 * Service layer for work with levelSet.
 */
@Service
public class LevelService {

    @Autowired
    private LevelDao levelDao;

    public String getLevelInfo(String level) {
        JSONArray jsonArray = new JSONArray();
        if (levelDao.contains(level)) {
            List<Result> resultList = new ArrayList<>(levelDao.getResults(level));
            Collections.sort(resultList, ResultComparator.getInstance());
            for (Result result : resultList) {
                if (jsonArray.size() < 20) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("user", result.getUser());
                    jsonObj.put("result", result.getResult());
                    jsonArray.add(jsonObj);
                } else {
                    break;
                }
            }
        } else {
            levelDao.setLevel(level);
        }
        return jsonArray.toJSONString();
    }
}
