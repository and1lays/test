/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.service;

import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.noosphere.dao.LevelDao;
import ua.yatsenko.noosphere.dao.SettingDao;
import ua.yatsenko.noosphere.dao.UserDao;
import ua.yatsenko.noosphere.dto.Result;
import ua.yatsenko.noosphere.utils.KeyComparator;

/**
 * 
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 * 
 * Service layer for setting info in resultMap.
 */
@Service
public class SettingService{

    @Autowired
    private SettingDao settingDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LevelDao levelDao;
    
    public void putInfo(String info) {
        try {

            JSONParser parser = new JSONParser();
            JSONObject jsonResult = (JSONObject) parser.parse(info);
            Result result = new Result((String) jsonResult.get("user"), (String) jsonResult.get("level"), (long) jsonResult.get("result"));

            long nextResultKey = 0;
            if (!settingDao.isEmpty()) {
                nextResultKey = Collections.max(settingDao.keySet(), KeyComparator.getInstance()) + 1;
            }
            boolean flag = true;

            settingDao.lock(nextResultKey);
            
            try {
                for (Long key : settingDao.keySet()) {
                    if (settingDao.containsValue(result) || flag == false) {
                        flag = false;
                        break;
                    }

                    Result res = settingDao.getByKey(key);
                    if (res.getUser().equals(result.getUser()) && res.getLevel().equals(result.getLevel())) {
                        if (res.getResult() < result.getResult()) {
                            settingDao.replace(key, res, result);
                        }
                        flag = false;
                    }
                }
                if (flag) {
                    settingDao.set(nextResultKey, result);
                    if (!userDao.contains(result.getUser())) {
                        userDao.setUser(result.getUser());
                    }
                    if (!levelDao.contains(result.getLevel())) {
                        levelDao.setLevel(result.getLevel());
                    }
                }
            } finally {
                settingDao.unlock(nextResultKey);
            }
        } catch (ParseException ex) {
            Logger.getLogger(SettingService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
