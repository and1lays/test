/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.dto;

import java.util.Comparator;

/**
 *
 * @author andrew
 */
public class KeyComparator implements Comparator<Integer>{

    @Override
    public int compare(Integer i1, Integer i2) {
        return (i1 - i2);
    }
    
}
