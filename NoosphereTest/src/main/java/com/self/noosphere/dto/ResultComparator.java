/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.dto;

import java.util.Comparator;

/**
 *
 * @author andrew
 */
public class ResultComparator implements Comparator<Result>{

    @Override
    public int compare(Result result1, Result result2) {
        return (int) (result2.getResult() - result1.getResult());
    }
    
}
