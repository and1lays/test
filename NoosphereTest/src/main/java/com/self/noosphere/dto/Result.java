/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author andrew
 */
public class Result implements Serializable{
    
    private String userId;
    private String levelId;
    private long result;

    public Result(String userId, String levelId, long result) {
        this.userId = userId;
        this.levelId = levelId;
        this.result = result;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.userId);
        hash = 89 * hash + Objects.hashCode(this.levelId);
        hash = 89 * hash + (int) (this.result ^ (this.result >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Result other = (Result) obj;
        if (this.result != other.result) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.levelId, other.levelId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + "userId=" + userId + ", levelId=" + levelId + ", result=" + result + ']';
    }
    
    public String toJSONString(){
        return "{userId\" : \"" + userId + "\", \"levelId\" : \"" + levelId + "\", \"result\" : " + result + "}";
    }
}
