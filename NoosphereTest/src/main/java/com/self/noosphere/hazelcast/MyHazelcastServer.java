/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.hazelcast;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.self.noosphere.dto.Result;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Ryuzaki
 */
public class MyHazelcastServer {
    
    private HazelcastInstance instance;
    
    public void startHazelcastInstance(){
        Config cfg = new ClasspathXmlConfig("hazelcast.xml");
        cfg.setProperty("hazelcast.logging.type", "slf4j");
        instance = Hazelcast.newHazelcastInstance(cfg);
    }
    
    public void initialize(){

        IMap<Integer, String> userMap = instance.getMap("userMap");
        IMap<Integer, String> levelMap = instance.getMap("levelMap");
        IMap<Integer, Result> resultMap = instance.getMap("resultMap");
    }
    
    public static void main(String[] args) {
        MyHazelcastServer server = new MyHazelcastServer();
        server.startHazelcastInstance();
        server.initialize();
    }
}
