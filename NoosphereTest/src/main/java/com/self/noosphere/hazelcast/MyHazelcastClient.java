/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.hazelcast;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;

/**
 *
 * @author Ryuzaki
 */
public class MyHazelcastClient {
    
    public HazelcastInstance startClient(){
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress("127.0.0.1");
        return HazelcastClient.newHazelcastClient(clientConfig);
    }
    
    public static void main(String[] args) {
        MyHazelcastClient client = new MyHazelcastClient();
        client.startClient();
    }
}
