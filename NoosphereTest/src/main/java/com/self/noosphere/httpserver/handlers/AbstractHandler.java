/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.httpserver.handlers;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.self.noosphere.hazelcast.MyHazelcastClient;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 *
 * @author Ryuzaki
 */
public abstract class AbstractHandler implements HttpHandler {

    private MyHazelcastClient client;
    
    protected HazelcastInstance getClient() {
        client = new MyHazelcastClient();
        return client.startClient();
    }

    protected static String getFromURI(String fullPath, String contextPath) {
        String tmpStr = fullPath.substring(contextPath.length() + 1);

        if (tmpStr.contains("/")) {
            return fullPath.substring(contextPath.length() + 1, contextPath.length() + 1 + tmpStr.indexOf("/"));
        } else {
            return fullPath.substring(contextPath.length() + 1);
        }
    }

    protected static void writeResponse(HttpExchange httpExchange, String response) throws IOException {
        httpExchange.sendResponseHeaders(200, response.length());
        try (OutputStream os = httpExchange.getResponseBody()) {
            os.write(response.getBytes(), 0, response.length());
        }
    }
}
