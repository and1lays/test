/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.httpserver.handlers;

import com.hazelcast.core.IMap;
import com.self.noosphere.dto.KeyComparator;
import com.self.noosphere.dto.Result;
import com.self.noosphere.dto.ResultComparator;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ryuzaki
 */
public class LevelInfo extends AbstractHandler {

    @Override
    public void handle(HttpExchange he) throws IOException {
        try {
            IMap<Integer, String> levelMap = getClient().getMap("levelMap");
            IMap<Integer, Result> resultMap = getClient().getMap("resultMap");

            List<Result> resultList = new ArrayList<>();
            JSONArray jsonArray = new JSONArray();

            String level = getFromURI(he.getRequestURI().getPath(), he.getHttpContext().getPath());
            int nextLevelKey = 0;
            
            if(!levelMap.isEmpty()){
                nextLevelKey = Collections.max(levelMap.keySet(), new KeyComparator()) + 1;
            }
            
            levelMap.lock(nextLevelKey);
            
            try {
                if (levelMap.containsValue(level)) {
                    for (Result result : resultMap.values()) {
                        if (result.getLevelId() == null ? level == null : result.getLevelId().equals(level)) {
                            resultList.add(result);
                        }
                    }
                    Collections.sort(resultList, new ResultComparator());
                    int counter = 0;
                    for (Result result : resultList) {
                        if (counter < 20) {
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("username", result.getUserId());
                            jsonObj.put("result", result.getResult());
                            jsonArray.add(jsonObj);
                        } else {
                            break;
                        }
                        counter++;
                    }
                } else {
                    levelMap.put(nextLevelKey, level);
                }
            } finally {
                levelMap.unlock(nextLevelKey);
            }
            writeResponse(he, jsonArray.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(LevelInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
