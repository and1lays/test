/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.httpserver.handlers;

import com.hazelcast.core.IMap;
import com.self.noosphere.dto.KeyComparator;
import com.self.noosphere.dto.Result;
import com.self.noosphere.dto.ResultComparator;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ryuzaki
 */
public class UserInfo extends AbstractHandler {

    @Override
    public void handle(HttpExchange he) throws IOException {
        try {
            IMap<Integer, String> userMap = getClient().getMap("userMap");
            IMap<Integer, Result> resultMap = getClient().getMap("resultMap");

            JSONArray jsonArray = new JSONArray();
            List<Result> resultList = new ArrayList<>();

            String username = getFromURI(he.getRequestURI().getPath(), he.getHttpContext().getPath());
            int nextUserKey = 0;
            
            if(!userMap.isEmpty()){
                nextUserKey = Collections.max(userMap.keySet(), new KeyComparator()) + 1;
            }
            userMap.lock(nextUserKey);
            try {
                if (userMap.values().contains(username)) {
                    for (Result result : resultMap.values()) {
                        if (username == null ? result.getUserId() == null : username.equals(result.getUserId())) {
                            resultList.add(result);
                        }
                    }
                    Collections.sort(resultList, new ResultComparator());
                    int counter = 0;
                    for (Result result : resultList) {
                        if (counter < 20) {
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("level", result.getLevelId());
                            jsonObj.put("result", result.getResult());
                            jsonArray.add(jsonObj);
                        } else {
                            break;
                        }
                        counter++;
                    }
                } else {
                    userMap.put(nextUserKey, username);
                }
            } finally {
                userMap.unlock(nextUserKey);
            }
            writeResponse(he, jsonArray.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(UserInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
