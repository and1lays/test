/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.httpserver;

import com.self.noosphere.httpserver.handlers.SetInfo;
import com.self.noosphere.httpserver.handlers.UserInfo;
import com.self.noosphere.httpserver.handlers.LevelInfo;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ryuzaki
 */
public class MyHttpServer {

    private static HttpServer server;

    public static void start() {
        try {
            server = HttpServer.create(new InetSocketAddress(8765), 0);

            server.createContext("/userinfo", new UserInfo());
            server.createContext("/levelinfo", new LevelInfo());
            server.createContext("/setinfo", new SetInfo());

            server.setExecutor(null);
            server.start();
        } catch (IOException ex) {
            Logger.getLogger(MyHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        start();
    }

}
