/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.self.noosphere.httpserver.handlers;

import com.hazelcast.core.IMap;
import com.self.noosphere.dto.KeyComparator;
import com.self.noosphere.dto.Result;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Ryuzaki
 */
public class SetInfo extends AbstractHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            if ("put".equalsIgnoreCase(exchange.getRequestMethod())) {
                JSONParser parser = new JSONParser();
                JSONObject jsonResult = (JSONObject) parser.parse(new BufferedReader(new InputStreamReader(exchange.getRequestBody())));

                Result result = new Result((String) jsonResult.get("userId"), (String) jsonResult.get("levelId"), (long) jsonResult.get("result"));

                IMap<Integer, Result> resultMap = getClient().getMap("resultMap");
                IMap<Integer, String> userMap = getClient().getMap("userMap");
                IMap<Integer, String> levelMap = getClient().getMap("levelMap");

                int nextResultKey = 0;
                int nextUserKey = 0;
                int nextLevelKey = 0;

                if (!userMap.isEmpty()) {
                    nextUserKey = Collections.max(userMap.keySet(), new KeyComparator()) + 1;
                }
                if (!levelMap.isEmpty()) {
                    nextLevelKey = Collections.max(levelMap.keySet(), new KeyComparator()) + 1;
                }
                if (!resultMap.isEmpty()) {
                    nextResultKey = Collections.max(resultMap.keySet(), new KeyComparator()) + 1;
                }
                boolean flag = true;

                resultMap.lock(nextResultKey);
                userMap.lock(nextUserKey);
                levelMap.lock(nextLevelKey);

                try {
                    for (Integer key : resultMap.keySet()) {
                        if (resultMap.containsValue(result) || flag == false) {
                            break;
                        }

                        Result res = resultMap.get(key);
                        if (res.getUserId().equals(result.getUserId()) && res.getLevelId().equals(result.getLevelId())) {
                            if (res.getResult() < result.getResult()) {
                                resultMap.replace(key, res, result);
                            }
                            flag = false;
                        }
                    }
                    if (flag) {
                        resultMap.put(nextResultKey, result);
                        if (!userMap.containsValue(result.getUserId())) {
                            userMap.put(nextUserKey, result.getUserId());
                        }
                        if (!levelMap.containsValue(result.getLevelId())) {
                            levelMap.put(nextLevelKey, result.getLevelId());
                        }
                    }
                } finally {
                    resultMap.unlock(nextResultKey);
                    userMap.unlock(nextUserKey);
                    levelMap.unlock(nextLevelKey);
                }
            }
        } catch (IOException | ParseException ex) {
            Logger.getLogger(SetInfo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            exchange.close();
        }

    }

}
