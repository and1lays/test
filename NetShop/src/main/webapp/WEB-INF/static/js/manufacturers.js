$(document).ready(function () {

    var host = window.location.protocol + "//" + window.location.host;

    var prefixUrl = host + '/NetShop/api/manufacturer/';

    $.getJSON(prefixUrl + "list")
            .then(jsonEach)
            .then(addManufacturer)
            .then(delManufacturer)
            .then(editManufacturer);

    function jsonEach(data) {
        $.each(data, function (index, manufacturer) {
            $("#man-body").append('<tr>\n\
                    <td>' + manufacturer.id + '</td>\n\
                    <td>' + manufacturer.name + '</td>\n\
                    <td><button type="submit" data-item-id="' + manufacturer.id + '" \n\
                        class="btn btn-danger delete-manufacturer" data-toggle="tooltip" title="Delete this manufacturer">\n\
                            <span class="glyphicon glyphicon-trash"></span></button>\n\
                    <button type="button" data-target="#editModal" data-item-id="' + manufacturer.id + '\" \n\
                        data-keyboard="true" class="btn btn-info edit-manufacturer" data-toggle="modal" title="Edit this manufacturer">\n\
                            <span class="glyphicon glyphicon-edit"></span></button></td>\n\
                    </tr>');
        });
    }

    function addManufacturer() {
        $("#add-manufacturer").click(function () {
            $.ajax({
                type: "POST",
                url: prefixUrl + "create",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                },
                data: JSON.stringify({name: $("#name-manufacturer").val()})
            });
        });
    }

    function delManufacturer() {
        $(".delete-manufacturer").click(function () {
            var deleteID = $(this).data('item-id');
            $.ajax({
                type: "DELETE",
                url: prefixUrl + 'id/' + deleteID,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                }
            });
        });
    }

    function editManufacturer() {
        $(function () {
            $(".edit-manufacturer").click(function () {
                editID = $(this).data('item-id');
                var url = prefixUrl + 'id/' + editID;
                $.getJSON(url, function (data) {
                    $(".get-manufacturer-name").val(data.name);
                });
            });
            $(".save-manufacturer").click(function () {
                $.ajax({
                    type: "PUT",
                    url: prefixUrl + 'id/' + editID,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({name: $(".get-manufacturer-name").val()}),
                    success: function () {
                    }
                });
                $("#editModal").modal("hide");
            });
        });
    }

});