$(document).ready(function () {

    var host = window.location.protocol + "//" + window.location.host;

    var productsUrl = host + '/NetShop/api/product/';
    
    var categoryId = $(".categories-filter option:selected").data('item-id');
    var manufacturerId = $(".manufacturers-filter option:selected").data('item-id');
    var lowPrice = $("#lowPrice").val();
    var highPrice = $("#highPrice").val();
    var isAvailable = $("#isAvailable:checked").length;
    
    var url =  productsUrl + "list?categ=" + categoryId + "&manuf=" + manufacturerId 
                    + "&avail=" + isAvailable + "&low=" + lowPrice + "&high=" + highPrice;
    
    var refreshUrl = host + '/NetShop/product';
    
    $.getJSON(url)
            .then(createProductTable)
            .then(addProduct)
            .then(delProduct)
            .then(editProduct);

    
    function createProductTable(data) {
        $.each(data, function (index, product) {
            $("#prod-body").append('<tr>\n\
                    <td>' + product.id + '</td>\n\
                    <td>' + product.name + '</td>\n\
                    <td>' + product.price + '</td>\n\
                    <td>' + product.isAvailable + '</td>\n\
                    <td>' + product.description + '</td>\n\
                    <td>' + product.category.name + '</td>\n\
                    <td>' + product.manufacturer.name + '</td>\n\
                    <td><button type="submit" data-item-id="' + product.id + '" \n\
                        class="btn btn-danger delete-product" data-toggle="tooltip" title="Delete this product">\n\
                            <span class="glyphicon glyphicon-trash"></span></button>\n\
                    <button type="button" data-target="#editModal" data-item-id="' + product.id + '\" \n\
                        data-keyboard="true" class="btn btn-info edit-product" data-toggle="modal" title="Edit this product">\n\
                            <span class="glyphicon glyphicon-edit"></span></button></td>\n\
                    </tr>');
        });
    }

    function addProduct() {
        $("#add-product").click(function () {
            $.ajax({
                type: "POST",
                url: productsUrl + 'create',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                },
                data: JSON.stringify({
                    name: $("#product-name").val(),
                    description: $("#product-description").val(),
                    price: $("#product-price").val(),
                    isAvailable: $("#product-isAvailable option:selected").val(),
                    category:{
                        id: $("#add-category-select option:selected").data('item-id')
                    },
                    manufacturer:{
                        id: $("#add-manufacturer-select option:selected").data('item-id')
                    }
                })
            });
        });
    }

    function delProduct() {
        $(".delete-product").click(function () {
            var deleteID = $(this).data('item-id');
            $.ajax({
                type: "DELETE",
                url: productsUrl + 'id/' + deleteID,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                }
            });
        });
    }

    function editProduct() {
        $(function () {
            $(".edit-product").click(function () {
                editID = $(this).data('item-id');
                var url = productsUrl + 'id/' + editID;
                $.getJSON(url, function (data) {
                    $("#get-product-name").val(data.name);
                    $("#get-product-price").val(data.price);
                    $("#get-product-description").val(data.description);
                    $('#get-product-isavailable').find('option:contains('+data.isAvailable+')').prop("selected", true);
                    $('#edit-categories-dropdown').find('option[data-item-id="' + data.category.id + '"]').prop("selected", true);
                    $('#edit-manufacturers-dropdown').find('option[data-item-id="' + data.manufacturer.id + '"]').prop("selected", true);
                });
            });
            $(".save-product").click(function () {
                console.log($("select#edit-categories-dropdown").find(":selected").data('item-id'));
                console.log($("select#edit-manufacturers-dropdown").find(":selected").data('item-id'));
                $.ajax({
                    type: "PUT",
                    url: productsUrl + 'id/' + editID,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        name: $("input#get-product-name").val(),
                        description: $("textarea#get-product-description").val(),
                        price: $("input#get-product-price").val(),
                        isAvailable: $("select#get-product-isavailable").find(":selected").val(),
                        category:{
                            id: $("select#edit-categories-dropdown").find(":selected").data('item-id')
                        },
                        manufacturer:{
                            id: $("select#edit-manufacturers-dropdown").find(":selected").data('item-id')
                        }
                    }),
                    success: function () {
                    }
                });
                $("#editModal").modal("hide");
            });
        });
    }
    
    refreshFilter();
    function refreshFilter(){
        $(".filter-btn").click(function (){
            categoryId = $(".categories-filter option:selected").data('item-id');
            manufacturerId = $(".manufacturers-filter option:selected").data('item-id');
            lowPrice = $("#lowPrice").val();
            highPrice = $("#highPrice").val();
            isAvailable = $("#isAvailable:checked").length;
            location.href = refreshUrl + "?categ=" + categoryId + "&manuf=" + manufacturerId 
                    + "&avail=" + isAvailable + "&low=" + lowPrice + "&high=" + highPrice;
        });
    }
});