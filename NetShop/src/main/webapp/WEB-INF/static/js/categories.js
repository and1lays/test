$(document).ready(function () {

    var host = window.location.protocol + "//" + window.location.host;
    var prefixUrl = host + '/NetShop/api/category/';

    $.getJSON(prefixUrl + "list")
            .then(tableCreate)
            .then(addCategory)
            .then(delCategory)
            .then(editCategory);

    function tableCreate(data) {
        $.each(data, function (index, category) {
            $("#cat-body").append('<tr>\n\
                    <td>' + category.id + '</td>\n\
                    <td>' + category.name + '</td>\n\
                    <td>' + category.idParent + '</td>\n\
                    <td><button type="submit" data-item-id="' + category.id + '" \n\
                        class="btn btn-danger delete-сategory" data-toggle="tooltip" title="Delete this category">\n\
                            <span class="glyphicon glyphicon-trash"></span></button>\n\
                    <button type="button" data-target="#editModal" data-item-id="' + category.id + '\" \n\
                        data-keyboard="true" class="btn btn-info edit-category" data-toggle="modal" title="Edit this category">\n\
                            <span class="glyphicon glyphicon-edit"></span></button></td>\n\
                    </tr>');
        });
    }

    function addCategory() {
        $("#add-category").click(function () {
            $.ajax({
                type: "POST",
                url: prefixUrl + 'create',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                },
                data: JSON.stringify({
                    name: $("#name-category").val(),
                    idParent: $("#add-parent option:selected").data('item-id')
        })
            });
        });
    }

    function delCategory() {
        $(".delete-сategory").click(function () {
            var deleteID = $(this).data('item-id');
            $.ajax({
                type: "DELETE",
                url: prefixUrl + 'id/' + deleteID,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function () {
                }
            });
        });
    }

    function editCategory() {
        $(function () {
            $(".edit-category").click(function () {
                editID = $(this).data('item-id');
                var url = prefixUrl + 'id/' + editID;
                $.getJSON(url, function (data) {
                    $(".get-category-name").val(data.name);
                });
            });
            $(".save-category").click(function () {
                $.ajax({
                    type: "PUT",
                    url: prefixUrl + 'id/' + editID,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        name: $(".get-category-name").val(),
                        idParent: $("#edit-parent option:selected").data('item-id')
                    }),
                    success: function () {
                    }
                });
                $("#editModal").modal("hide");
            });
        });
    }

});