/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dto;

import java.util.Objects;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class Category {
    private long id;
    private String name;
    private long idParent;

    public Category(long id, String name, long idParent) {
        this.id = id;
        this.name = name;
        this.idParent = idParent;
    }

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(long id) {
        this.id = id;
    }

    public Category() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdParent() {
        return idParent;
    }

    public void setIdParent(long idParent) {
        this.idParent = idParent;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + (int) (this.idParent ^ (this.idParent >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.idParent != other.idParent) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + "id=" + id + ", name=" + name + ", idParent=" + idParent + "]";
    }
    
    
    
}
