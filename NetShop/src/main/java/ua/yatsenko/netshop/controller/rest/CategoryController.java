/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.controller.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.yatsenko.netshop.dto.Category;
import ua.yatsenko.netshop.service.CategoryService;



/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@RestController
@RequestMapping("/api/category")
public class CategoryController {
    
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Category> getAll() {
        return categoryService.getAll();
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public Category getById(@PathVariable("id") long id) {
        return categoryService.getItemById(id);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public Category getByName(@PathVariable("name") String name) {
        return categoryService.getItemByName(name);
    }
    
    @RequestMapping(value = "/id/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") long id, @RequestBody Category category){
        categoryService.updateItem(category, id);
    }
    
    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {
        categoryService.deleteItemById(id);
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void create(@RequestBody Category category){
        categoryService.addItem(category);
    }
    
}
