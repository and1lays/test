/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.controller.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.yatsenko.netshop.dto.Manufacturer;
import ua.yatsenko.netshop.service.ManufacturerService;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@RestController
@RequestMapping("/api/manufacturer")
public class ManufacturerController {

    @Autowired
    private ManufacturerService manufacturerService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Manufacturer> getAll() {
        return manufacturerService.getAll();
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public Manufacturer getById(@PathVariable("id") long id) {
        return manufacturerService.getItemById(id);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public Manufacturer getByName(@PathVariable("name") String name) {
        return manufacturerService.getItemByName(name);
    }
    
    @RequestMapping(value = "/id/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") long id, @RequestBody Manufacturer manufacturer){
        manufacturerService.updateItem(manufacturer, id);
    }
    
    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {
        manufacturerService.deleteItemById(id);
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void create(@RequestBody Manufacturer manufacturer){
        manufacturerService.addItem(manufacturer);
    }
}
