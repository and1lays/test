/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.controller.rest;

import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.yatsenko.netshop.dto.Product;
import ua.yatsenko.netshop.service.ProductService;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@RestController
@RequestMapping("/api/product")
public class ProductController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Product> getProducts(
            @RequestParam(name = "avail", required = false, defaultValue = "false") Boolean isAvailable,
            @RequestParam(name = "low", required = false) BigDecimal lowPrice,
            @RequestParam(name = "high", required = false) BigDecimal highPrice,
            @RequestParam(name = "categ", required = false, defaultValue = "0") Long categoryID,
            @RequestParam(name = "manuf", required = false, defaultValue = "0") Long manufacturerID
    ) {
        return productService.getAll(isAvailable, lowPrice, highPrice, categoryID, manufacturerID);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public Product getById(@PathVariable("id") Long id) {
        return productService.getItemById(id);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public Product getByName(@PathVariable("name") String name) {
        return productService.getItemByName(name);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") Long id, @RequestBody Product product) {
        productService.updateItem(product, id);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        productService.deleteItemById(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void create(@RequestBody Product product) {
        productService.addItem(product);
    }
}
