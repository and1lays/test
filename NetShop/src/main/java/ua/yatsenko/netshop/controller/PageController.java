/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.yatsenko.netshop.dto.Category;
import ua.yatsenko.netshop.dto.Manufacturer;
import ua.yatsenko.netshop.service.CategoryService;
import ua.yatsenko.netshop.service.ManufacturerService;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Controller()
public class PageController {
    
    @Autowired
    private CategoryService categoryService;
    
    @Autowired
    private ManufacturerService manufacturerService;

    @RequestMapping({"/", "/index"})
    public String index() {
        return "html/index";
    }

    @RequestMapping("/category")
    public ModelAndView category() {
        ModelAndView mav = new ModelAndView("html/categories");
        List<Category> categories = categoryService.getAll();
        mav.addObject("categories", categories);
        return mav;
    }

    @RequestMapping("/manufacturer")
    public String manufacturer() {
        return "html/manufacturers";
    }

    @RequestMapping("/product")
    public ModelAndView product(
            @RequestParam(name = "avail", required = false, defaultValue = "false") Boolean isAvailable,
            @RequestParam(name = "low", required = false) BigDecimal lowPrice,
            @RequestParam(name = "high", required = false) BigDecimal highPrice,
            @RequestParam(name = "categ", required = false, defaultValue = "0") Long categoryID,
            @RequestParam(name = "manuf", required = false, defaultValue = "0") Long manufacturerID
    ) {
        ModelAndView mav = new ModelAndView("html/products");
        
        List<Manufacturer> manufacturers = new ArrayList<>();
        manufacturers.add(new Manufacturer(0, "Все производители"));
        manufacturers.addAll(manufacturerService.getAll());
        
        List<Category> categories = categoryService.getAll();
        
        mav.addObject("categories", categories);
        mav.addObject("categorySelected", categoryService.getSelectedCategory(categoryID));
        mav.addObject("manufacturers", manufacturers);
        mav.addObject("manufacturerSelected", manufacturerService.getSelectedManufacturer(manufacturerID));
        mav.addObject("lowPrice", lowPrice);
        mav.addObject("highPrice", highPrice);
        mav.addObject("availableStatus", isAvailable);
        
        return mav;
    }
}
