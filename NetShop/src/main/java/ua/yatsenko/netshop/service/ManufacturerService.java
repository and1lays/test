/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.netshop.dao.ManufacturerDao;
import ua.yatsenko.netshop.dto.Manufacturer;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Service
public class ManufacturerService implements IService<Manufacturer> {

    @Autowired
    private ManufacturerDao manufacturerDao;

    @Override
    public Manufacturer getItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        return manufacturerDao.getById(id);
    }

    @Override
    public Manufacturer getItemByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Incorrect Name!");
        }
        Manufacturer manufacturer = manufacturerDao.getByName(name);
        return manufacturer;
    }

    public String getSelectedManufacturer(Long manufacturerID) {
        if (manufacturerID == 0) {
            return "Все производители";
        } else {
            return getItemById(manufacturerID).getName();
        }
    }

    public List<Manufacturer> getAll() {
        return manufacturerDao.getAll();
    }

    @Override
    public void addItem(Manufacturer manufacturer) {
        synchronized (manufacturer) {
            if (manufacturer.getName() == null || manufacturer.getName().isEmpty()) {
                throw new IllegalArgumentException("Incorrect Name!");
            }
            if (manufacturerDao.getByName(manufacturer.getName()) != null) {
                try {
                    throw new Exception("Manufacturer " + manufacturer.getName() + " already exists!");
                } catch (Exception ex) {
                    Logger.getLogger(ManufacturerService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                manufacturerDao.insert(manufacturer);
            }
        }
    }

    @Override
    public void updateItem(Manufacturer manufacturer, long id) {
        Manufacturer manuf = manufacturerDao.getById(id);
        synchronized (manuf) {
            if (manuf == null) {
                throw new IllegalArgumentException("There is no manufacturer with id: " + id);
            }
            if (manuf.getName() != null) {
                manuf.setName(manufacturer.getName());
            }
            manufacturerDao.update(manuf);
        }
    }

    @Override
    public void deleteItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        manufacturerDao.deleteById(id);
    }

}
