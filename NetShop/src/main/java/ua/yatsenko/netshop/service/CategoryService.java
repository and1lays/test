/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.netshop.dao.CategoryDao;
import ua.yatsenko.netshop.dto.Category;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Service
public class CategoryService implements IService<Category> {

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public Category getItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        return categoryDao.getById(id);
    }

    @Override
    public Category getItemByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Incorrect Name!");
        }
        Category category = categoryDao.getByName(name);
        return category;
    }

    public String getSelectedCategory(Long categoryID) {
        if (categoryID == 0) {
            return "Все производители";
        } else {
            return getItemById(categoryID).getName();
        }
    }

    public List<Category> getAll() {
        return categoryDao.getAll();
    }

    @Override
    public void addItem(Category category) {
        synchronized (category) {
            if (category.getName() == null || category.getName().isEmpty()) {
                throw new IllegalArgumentException("Incorrect Name!");
            }
            if (categoryDao.getByName(category.getName()) != null) {
                try {
                    throw new Exception("Category " + category.getName() + " already exists!");
                } catch (Exception ex) {
                    Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            categoryDao.insert(category);
        }
    }

    @Override
    public void updateItem(Category category, long id) {
        Category cat = categoryDao.getById(id);
        synchronized (cat) {
            if (cat == null) {
                throw new IllegalArgumentException("There is no category with id: " + id);
            }
            if (cat.getName() != null) {
                cat.setName(category.getName());
            }
            cat.setIdParent(category.getIdParent());
            categoryDao.update(cat);
        }
    }

    @Override
    public void deleteItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        categoryDao.deleteById(id);
    }
}
