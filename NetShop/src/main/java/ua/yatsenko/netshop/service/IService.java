/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.service;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public interface IService<T> {
    T getItemById(long id);

    T getItemByName(String name);

    void addItem(T item);

    void updateItem(T item, long id);

    void deleteItemById(long id);
}
