/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.yatsenko.netshop.dao.CategoryDao;
import ua.yatsenko.netshop.dao.ManufacturerDao;
import ua.yatsenko.netshop.dao.ProductDao;
import ua.yatsenko.netshop.dto.Product;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Service
public class ProductService implements IService<Product> {

    @Autowired
    private ProductDao productDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private ManufacturerDao manufacturerDao;

    @Override
    public Product getItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        Product product = productDao.getById(id);
        product.setCategory(categoryDao.getById(product.getCategory().getId()));
        product.setManufacturer(manufacturerDao.getById(product.getManufacturer().getId()));
        return product;

    }

    @Override
    public Product getItemByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Incorrect Name!");
        }
        Product product = productDao.getByName(name);
        product.setCategory(categoryDao.getById(product.getCategory().getId()));
        product.setManufacturer(manufacturerDao.getById(product.getManufacturer().getId()));
        return product;
    }

    public List<Product> getAll(Boolean isAvailable, BigDecimal lowPrice, BigDecimal highPrice, Long categoryID, Long manufacturerID) {
        List<Product> productList;

        if (categoryID == 0) {
            categoryID = categoryDao.getByName("Все категории").getId();
        }
        if (lowPrice == null) {
            lowPrice = productDao.getLowPrice();
        }
        if (highPrice == null) {
            highPrice = productDao.getHighPrice();
        }

        if (manufacturerID == 0) {
            if (isAvailable == true) {
                productList = productDao.getAvailableProductsWithoutManufacturerID(lowPrice, highPrice, categoryID);
            } else {
                productList = productDao.getProductsWithoutManufacturerID(lowPrice, highPrice, categoryID);
            }
        } else {
            if (isAvailable == true) {
                productList = productDao.getAvailableProducts(lowPrice, highPrice, categoryID, manufacturerID);
            } else {
                productList = productDao.getProducts(lowPrice, highPrice, categoryID, manufacturerID);
            }
        }
        for (Product product : productList) {
            product.setCategory(categoryDao.getById(product.getCategory().getId()));
            product.setManufacturer(manufacturerDao.getById(product.getManufacturer().getId()));
        }
        return productList;
    }

    @Override
    public void addItem(Product product) {
        if (product.getName() == null || product.getName().isEmpty()) {
            throw new IllegalArgumentException("Incorrect Name!");
        }
        synchronized (product) {
            if (productDao.getByName(product.getName()) != null) {
                try {
                    throw new Exception("Product " + product.getName() + " already exists!");
                } catch (Exception ex) {
                    Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            productDao.insert(product);
        }
    }

    @Override
    public void updateItem(Product product, long id) {

        Product prod = productDao.getById(id);
        if (prod == null) {
            throw new IllegalArgumentException("There is no product with id: " + id);
        }
        synchronized (prod) {
            if (prod.getName() != null) {
                prod.setName(product.getName());
                prod.setPrice(product.getPrice());
                prod.setDescription(product.getDescription());
                prod.setIsAvailable(product.isIsAvailable());
                prod.setCategory(product.getCategory());
                prod.setManufacturer(product.getManufacturer());
            }
            productDao.update(prod);
        }
    }

    @Override
    public void deleteItemById(long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Incorrect ID!");
        }
        productDao.deleteById(id);
    }
}
