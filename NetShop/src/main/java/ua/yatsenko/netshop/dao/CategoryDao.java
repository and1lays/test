/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.yatsenko.netshop.dao.mapper.CategoryRowMapper;
import ua.yatsenko.netshop.dto.Category;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class CategoryDao implements IDao<Category> {

    private static final String SELECT_ALL_FIELDS = "select id, name, id_parent from category";

    @Autowired
    private JdbcTemplate getJdbcTemplate;

    @Override
    public Category getById(long id) {
        try {
            return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where id=?", new CategoryRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Category getByName(String name) {
        try {
            return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where name=?", new CategoryRowMapper(), name);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Category> getAll() {
        return getJdbcTemplate.query(SELECT_ALL_FIELDS, new CategoryRowMapper());
    }

    public Category getParentCategory(long idParent) {
        return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where id=?", new CategoryRowMapper(), idParent);
    }

    @Override
    public void insert(Category category) {
        getJdbcTemplate.update("insert into category(name, id_parent) values(?,?)", category.getName(), category.getIdParent());
    }

    @Override
    public void update(Category category) {
        getJdbcTemplate.update("update category set name=?, id_parent=? where id=?", category.getName(), category.getIdParent(), category.getId());
    }

    @Override
    public void deleteById(long id) {
        getJdbcTemplate.update("delete from category where id=?", id);
    }

}
