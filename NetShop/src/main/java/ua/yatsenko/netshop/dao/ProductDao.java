/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.yatsenko.netshop.dao.mapper.ProductRowMapper;
import ua.yatsenko.netshop.dto.Product;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class ProductDao implements IDao<Product>{

    private static final String SELECT_ALL_FIELDS = "select id, name, price, is_available, description, id_category, id_manufacturer from product";
    
    @Autowired
    private JdbcTemplate getJdbcTemplate;
    
    @Override
    public Product getById(long id) {
        return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where id=?", new ProductRowMapper(), id);
    }

    @Override
    public Product getByName(String name) {
        try{
            return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where name=?", new ProductRowMapper(), name);
        } catch(EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public List<Product> getAll() {
        return getJdbcTemplate.query(SELECT_ALL_FIELDS, new ProductRowMapper());
    }

    @Override
    public void insert(Product product) {
        getJdbcTemplate.update("insert into product(name, price, is_available, description, id_category, id_manufacturer) values(?,?,?,?,?,?)",
                product.getName(), product.getPrice(), product.isIsAvailable(), 
                product.getDescription(), product.getCategory().getId(), product.getManufacturer().getId());
    }

    @Override
    public void update(Product product) {
        getJdbcTemplate.update("update product set name=?, price=?, is_available=?, description=?, id_category=?, id_manufacturer=? where id=?",
                product.getName(), product.getPrice(), product.isIsAvailable(), product.getDescription(),
                product.getCategory().getId(), product.getManufacturer().getId(), product.getId());
    }

    @Override
    public void deleteById(long id) {
        getJdbcTemplate.update("delete from product where id=?", id);
    }
    
    public List<Product> getAvailableProductsWithoutManufacturerID(BigDecimal low, BigDecimal high, Long idCategory){
        return getJdbcTemplate.query(SELECT_ALL_FIELDS + " where is_available=true AND price between ? and ? and id_category in (select id from get_all_childs(?))",
                new ProductRowMapper(), low, high, idCategory);
    }
    
    public List<Product> getProductsWithoutManufacturerID(BigDecimal low, BigDecimal high, Long idCategory){
        return getJdbcTemplate.query(SELECT_ALL_FIELDS + " where price between ? and ? and id_category in (select id from get_all_childs(?))",
                new ProductRowMapper(), low, high, idCategory);
    }
    
    public List<Product> getProducts(BigDecimal low, BigDecimal high, Long idCategory, Long idManufacturer){
        return getJdbcTemplate.query(SELECT_ALL_FIELDS + " where price between ? and ? and id_manufacturer=? and id_category in (select id from get_all_childs(?))",
                new ProductRowMapper(), low, high, idManufacturer, idCategory);
    }
    
    public List<Product> getAvailableProducts(BigDecimal low, BigDecimal high, Long idCategory, Long idManufacturer){
        return getJdbcTemplate.query(SELECT_ALL_FIELDS + " where is_available=true AND price between ? and ? and id_manufacturer=? and id_category in (select id from get_all_childs(?))",
                new ProductRowMapper(), low, high, idManufacturer, idCategory);
    }
    
    public BigDecimal getLowPrice(){
        List<Map<String, Object>> rows = getJdbcTemplate.queryForList("select min(price) as min_price from product");
        BigDecimal lowPrice = null;
        for(Map row : rows){
            lowPrice = (BigDecimal) row.get("min_price");
        }
        return lowPrice;
    }
    
    public BigDecimal getHighPrice(){
        List<Map<String, Object>> rows = getJdbcTemplate.queryForList("select max(price) as max_price from product");
        BigDecimal highPrice = null;
        for(Map row : rows){
            highPrice = (BigDecimal) row.get("max_price");
        }
        return highPrice;
    }
}
