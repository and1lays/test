/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ua.yatsenko.netshop.dto.Category;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class CategoryRowMapper implements RowMapper<Category>{

    @Override
    public Category mapRow(ResultSet rs, int i) throws SQLException {
        Category category = new Category();
        category.setId(rs.getLong("id"));
        category.setName(rs.getString("name"));
        category.setIdParent(rs.getLong("id_parent"));
        return category;
    }
    
}
