/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.yatsenko.netshop.dao.mapper.ManufacturerRowMapper;
import ua.yatsenko.netshop.dto.Manufacturer;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class ManufacturerDao implements IDao<Manufacturer> {

    private static final String SELECT_ALL_FIELDS = "select id, name from manufacturer";

    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    @Autowired
    private JdbcTemplate getJdbcTemplate;

    @Override
    public Manufacturer getById(long id) {
        try {
            return getJdbcTemplate.queryForObject(SELECT_ALL_FIELDS + " where id=?", new ManufacturerRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Manufacturer getByName(String name) {

        try {
            Class.forName(driver).newInstance();
            try (Connection connection = DriverManager.getConnection(url, username, password)) {

                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FIELDS + " where name=?");
                preparedStatement.setString(1, name);

                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    Manufacturer manufacturer = new Manufacturer();

                    while (resultSet.next()) {
                        manufacturer.setId(resultSet.getLong("id"));
                        manufacturer.setName(resultSet.getString("name"));
                    }
                    return manufacturer;
                } else {
                    return null;
                }
            } catch (SQLException ex) {
                Logger.getLogger(ManufacturerDao.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ManufacturerDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public List<Manufacturer> getAll() {
        return getJdbcTemplate.query(SELECT_ALL_FIELDS, new ManufacturerRowMapper());
    }

    @Override
    public void insert(Manufacturer manufacturer) {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            Class.forName(driver).newInstance();

            PreparedStatement preparedStatement = connection.prepareStatement("insert into manufacturer(name) values(?)");
            preparedStatement.setString(1, manufacturer.getName());

            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(ManufacturerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Manufacturer manufacturer) {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            Class.forName(driver).newInstance();

            PreparedStatement preparedStatement = connection.prepareStatement("update manufacturer set name=? where id=?");
            preparedStatement.setString(1, manufacturer.getName());
            preparedStatement.setLong(2, manufacturer.getId());

            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(ManufacturerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteById(long id) {
        getJdbcTemplate.update("delete from manufacturer where id=?", id);
    }

}
