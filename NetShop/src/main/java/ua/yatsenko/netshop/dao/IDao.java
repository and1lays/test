/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao;

import java.util.List;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public interface IDao<T> {
    
    T getById(long id);

    T getByName(String name);

    List<T> getAll();

    void insert(T object);

    void update(T object);

    void deleteById(long id);
}
