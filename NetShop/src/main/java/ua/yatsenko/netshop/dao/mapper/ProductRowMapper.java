/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ua.yatsenko.netshop.dto.Category;
import ua.yatsenko.netshop.dto.Manufacturer;
import ua.yatsenko.netshop.dto.Product;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ProductRowMapper implements RowMapper<Product> {
    
    @Override
    public Product mapRow(ResultSet rs, int i) throws SQLException {
        Product product = new Product();
        product.setId(rs.getLong("id"));
        product.setName(rs.getString("name"));
        product.setPrice(rs.getDouble("price"));
        product.setIsAvailable(rs.getBoolean("is_available"));
        product.setDescription(rs.getString("description"));
        product.setCategory(new Category(rs.getLong("id_category")));
        product.setManufacturer(new Manufacturer(rs.getLong("id_manufacturer")));
        return product;
    }

}
