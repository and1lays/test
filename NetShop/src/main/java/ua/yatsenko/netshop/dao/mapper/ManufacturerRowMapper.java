/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.netshop.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ua.yatsenko.netshop.dto.Manufacturer;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ManufacturerRowMapper implements RowMapper<Manufacturer>{

    @Override
    public Manufacturer mapRow(ResultSet rs, int i) throws SQLException {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(rs.getLong("id"));
        manufacturer.setName(rs.getString("name"));
        return manufacturer;
    }
    
}
