/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Andrew Yatsenko <and1lays91@gmail.com>
 * Created: 13.04.2017
 */

CREATE DATABASE netshop WITH OWNER java;
CREATE TABLE category (
    id serial PRIMARY KEY,
    name character varying(30) NOT NULL,
    id_parent integer
);
alter table category add constraint parent_fk foreign key(id_parent) references category(id) match full;
CREATE TABLE manufacturer (
    id serial PRIMARY KEY,
    name character varying(30) NOT NULL
);
CREATE TABLE product (
    id serial PRIMARY KEY,
    name character varying(30) NOT NULL,
    price numeric(14,2) NOT NULL,
    is_available boolean NOT NULL,
    description text,
    id_category integer,
    id_manufacturer integer
);
alter table product add constraint category_fk foreign key(id_category) references category(id) match full;
alter table product add constraint manufacturer_fk foreign key(id_manufacturer) references manufacturer(id) match full;

CREATE FUNCTION get_all_childs(parent bigint) RETURNS TABLE(id integer)
    LANGUAGE plpgsql
    AS $$
declare 
        i integer;
        records record;
begin
        i := 1;
        create temporary table tmp (id int, status int);
        if parent = 0 then
                insert into tmp select category.id, 0 from category where category.id_parent is null;
        else
                insert into tmp values(parent, 0);
                insert into tmp select category.id, 0 from category where category.id_parent = parent;
        end if;
        while i > 0 loop
                perform tmp.id from tmp where tmp.status = 0;
                GET DIAGNOSTICS i = ROW_COUNT;
                for records in select tmp.id from tmp where tmp.status = 0 loop
                        insert into tmp select category.id, 0 from category where category.id_parent = records.id;
                        update tmp set status=1 where tmp.id = records.id;
                        return query 
                                select category.id
                                from category
                                where category.id = records.id;
                end loop;
        end loop;
        drop table tmp;
end;
$$;