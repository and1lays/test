/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.yatsenko.noosphere.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class Client implements Runnable{
    public static void main(String[] args) throws UnsupportedEncodingException, IOException {
        Client c1 = new Client();
        Client c2 = new Client();
        
        Thread t1 = new Thread(c1);
        Thread t2 = new Thread(c2);
        
        t1.start();
        t2.start();
        
    }

    @Override
    public void run() {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPut put  = new HttpPut("http://localhost:8084/NoosphereNextGen/setinfo");
//            HttpGet get  = new HttpGet("http://localhost:8084/NoosphereNextGen/userinfo/asd1");
            StringEntity entity = new StringEntity("{\"user\":\"first\", \"level\":\"first\", \"result\":9999999}");
            put.setEntity(entity);
            
            HttpResponse response = client.execute(put);
//            client.execute(get);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
