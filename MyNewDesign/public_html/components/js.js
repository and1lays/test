$(document).ready(function () {
    $('.expandable').click(function () {
        var id = $(this).parent().attr('id');
        if($(this).parent().hasClass('active')){
            $('#' + id + ' .panel-tooltips').css('display', 'none');
            $('#' + id + ' .panel-content').css('display', 'none');
            
            $(this).parent().removeClass('active');
        } else {
            $('#' + id + ' .panel-tooltips').css('display', 'block');
            $('#' + id + ' .panel-content').css('display', 'block');
            
            $(this).parent().addClass('active');
        }
//        if($('#' + id + ' .panel-content').css('display') === 'none'){
//            
//        } else {
//            
//        }
    });
});